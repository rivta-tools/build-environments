#!/bin/sh

OUTPUT_DIR="$PWD/vt-output"

# Copy sources
cp -r /usr/share/vt-gen $OUTPUT_DIR

# Make sure serviceArchetype is installed
cd $OUTPUT_DIR/virtualServiceArchetype && mvn clean install

# Run VirtualiseringGenerator
cd  $OUTPUT_DIR/scriptTemplate
./VirtualiseringGenerator.groovy $1 $2 $3
mvn clean package
