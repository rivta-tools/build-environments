# README #

Innehåller gemensamma komponenter i bygg- och leveranskedja för RIV TA tjänstedomäner. Repositoryt består huvudsakligen av två delar, Docker-containers och Pipeline-definitioner.

## rivta-domains ##

Definition av en Jenkins-pipeline för att bygga och testa RIV TA tjänstedomäner. Det finns även en legacy-version, med färre tester. Den används för domäner som är skapade efter äldre regelverk.

## Containers ##

Docker-containers som används för de olika bygg- och teststegen. Dessa containers byggs med hjälp av den Jenkinsfile som ligger roten av repositoryt. 
Bygget uppdaterar även Ineras Docker-registry - https://docker.drift.inera.se

### verify-scripts ###

Miljö för att köra RIV TA verifierings- och paketeringsscript - https://bitbucket.org/rivta-tools/verify-scripts

### vt-gen ###

Miljö för att skapa virtuella tjänster för SKLTP för en tjänstedomän. Groovy och Maven används huvudsakligen, se https://skl-tp.atlassian.net/wiki/pages/viewpage.action?pageId=36372555